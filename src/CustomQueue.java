import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

public class CustomQueue {
    public static void main(String[] args) {

        Queue<Customer> cq = new PriorityQueue<>(categoryCompare);

        add(cq);

        poll(cq);

    }

    public static Comparator<Customer> categoryCompare = Comparator.comparing(Customer::getCategory);

    private static void add(Queue<Customer> cq) {

        cq.add(new Customer(1, "B"));
        cq.add(new Customer(2, "C"));
        cq.add(new Customer(3, "A"));
        cq.add(new Customer(4, "C"));
        cq.add(new Customer(5, "A"));
        cq.add(new Customer(6, "B"));

    }

    private static void poll(Queue<Customer> cq) {
        while (true) {
            Customer customer = cq.poll();
            if (customer == null) break;
            System.out.println("Processing = " + customer);
        }
    }

}
