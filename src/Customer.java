public class Customer {
    private int number;
    private String category;

    public Customer(int number, String category) {
        this.number = number;
        this.category = category;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "number=" + number +
                ", category='" + category + '\'' +
                '}';
    }

}
